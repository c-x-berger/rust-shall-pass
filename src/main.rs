use hmac::{Hmac, Mac, NewMac};
use sha2::Sha512;
use structopt::StructOpt;

type HmacSha512 = Hmac<Sha512>;

#[derive(StructOpt)]
#[structopt(name = "rsp")]
struct Opt {
    #[structopt(short, long)]
    passphrase: String,

    #[structopt(short, long)]
    email: String,

    #[structopt(short, long, default_value = "8")]
    iterations: u8,

    #[structopt(short, long)]
    host: String,

    #[structopt(short, long, default_value = "1")]
    generation: usize,

    #[structopt(short, long, default_value = "12")]
    length: usize,
}

fn main() {
    let opts = Opt::from_args();
    let mut derived_key: [u8; 64] = [0; 64];
    pbkdf2::pbkdf2::<HmacSha512>(
        opts.passphrase.as_bytes(),
        opts.email.as_bytes(),
        2_u32.pow(opts.iterations as u32),
        &mut derived_key,
    );

    let mut demands_iter = 0;
    let mut mac = HmacSha512::new_varkey(&derived_key).unwrap();
    let encoded_pw = loop {
        let data = format!(
            r#"rust-shall-pass v0.1; {}; {}; {}; {}"#,
            opts.email, opts.host, opts.generation, demands_iter
        );
        mac.update(data.as_bytes());
        let res_bytes = mac.finalize_reset();
        let encoded = base64::encode(res_bytes.into_bytes().as_slice());
        if demands_check(&encoded) {
            break encoded;
        } else {
            demands_iter += 1;
        }
    };
    println!("{}", &encoded_pw[0..opts.length])
}

fn demands_check(test: &str) -> bool {
    let mut uppper_count = 0;
    let mut lower_count = 0;
    let mut digit_count = 0;
    let relevant_chars = test[0..16].chars();
    for (idx, ch) in relevant_chars.enumerate() {
        if idx <= 7 {
            if ch.is_ascii_uppercase() {
                uppper_count += 1;
            }
            if ch.is_ascii_lowercase() {
                lower_count += 1;
            }
            if ch.is_ascii_digit() {
                digit_count += 1;
            }
        }
        if ch == '+' || ch == '/' || ch == '=' {
            return false;
        }
    }
    return !bad(uppper_count) && !bad(lower_count) && !bad(digit_count);
}

fn bad(count: usize) -> bool {
    return count == 0 || count > 5;
}
